package day06;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Calc_PageFactory_User_NG {
	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome");
	}

	@Test
	public void testMul() {

		driver.get("http://ata123456789123456789.appspot.com/");
		String expRes = "150";
		String actRes="";

		Calc_PageFactory ataPage = new Calc_PageFactory(driver);
		//Ex14_ATA_Calc_POM ataPage = new Ex14_ATA_Calc_POM();
		

		actRes = ataPage.multiply("10", "15");
		System.out.println("The Result is = " + actRes + "   ...   ");

		Assert.assertEquals(actRes, expRes, "Incorrect Result from Application \n");

	}
	
	@Test
	public void testAdd() {

		driver.get("http://ata123456789123456789.appspot.com/");
		String expRes = "46";
		String actRes="";

		Calc_PageFactory ataPage = new Calc_PageFactory(driver);
		//Ex14_ATA_Calc_POM ataPage = new Ex14_ATA_Calc_POM();
		

		actRes = ataPage.addition("12", "34");
		System.out.println("The Result is = " + actRes + "   ...   ");

		Assert.assertEquals(actRes, expRes, "Incorrect Result from Application \n");

	}


	@AfterTest
	public void afterTest() {
		driver.quit();
	}


}
