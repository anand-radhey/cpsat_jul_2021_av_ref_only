package day06;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Ex14_ATA_Calc_POM {
	WebDriver driver;
	
	//*[@id="ID_nameField1"]
		//*[@id="ID_nameField2"]
		//*[@id="gwt-uid-2"]
		//*[@id="gwt-uid-1"]
		//*[@id="ID_calculator"]
		//*[@id="ID_nameField3"]
	
	By byField1 = By.xpath("//*[@id=\"ID_nameField1\"]");
	By byField2 = By.xpath("//*[@id=\"ID_nameField2\"]");
	By byMul = By.xpath("//*[@id=\"gwt-uid-2\"]");
	By byAdd = By.xpath("//*[@id=\"gwt-uid-1\"]");
	By byCalc = By.xpath("//*[@id=\"ID_calculator\"]");
	By byRes = By.xpath("//*[@id=\"ID_nameField3\"]");
	By bySqr = By.xpath("//*[@id=\"gwt-uid-3\"]");
	
	
	public Ex14_ATA_Calc_POM(WebDriver driver){
		this.driver = driver;
	}
	
	public String	multiply(String in1, String in2){
		String actRes = "";
		
		WebElement weField1 = driver.findElement(byField1);
		WebElement weField2 = driver.findElement(byField2);
		WebElement weMul = driver.findElement(byMul);
		WebElement weAdd = driver.findElement(byAdd);
		WebElement weSqr = driver.findElement(bySqr);
		
		
		WebElement weCalc = driver.findElement(byCalc);
		WebElement weRes = driver.findElement(byRes);
		
		weField1.clear();
		weField1.sendKeys( in1);
		weField2.clear();
		weField2.sendKeys(in2);
		
		weMul.click();
		weCalc.click();
		
		actRes =  weRes.getAttribute("value");   //weRes.getText();
		
		
		
		return actRes;
	}
	
	public String	addition(String in1, String in2){
		String actRes = "";

		WebElement weField1 = driver.findElement(byField1);
		WebElement weField2 = driver.findElement(byField2);
		WebElement weMul = driver.findElement(byMul);
		WebElement weAdd = driver.findElement(byAdd);
		WebElement weCalc = driver.findElement(byCalc);
		WebElement weRes = driver.findElement(byRes);
		
		weField1.clear();
		weField1.sendKeys( in1);
		weField2.clear();
		weField2.sendKeys(in2);
		
		weAdd.click();
		weCalc.click();
		
		actRes =  weRes.getAttribute("value");   //weRes.getText();
		return actRes;
	}
	
	public String	square(String in1, String in2){
		String actRes = "";

		WebElement weField1 = driver.findElement(byField1);
		WebElement weField2 = driver.findElement(byField2);
		WebElement weMul = driver.findElement(byMul);
		WebElement weAdd = driver.findElement(byAdd);
		WebElement weSqr = driver.findElement(bySqr);
		WebElement weCalc = driver.findElement(byCalc);
		WebElement weRes = driver.findElement(byRes);
		
		weField1.clear();
		weField1.sendKeys( in1);
		weField2.clear();
		weField2.sendKeys(in2);
		
		weSqr.click();
		weCalc.click();
		
		actRes =  weRes.getAttribute("value");   //weRes.getText();
		return actRes;
	}


	

}
