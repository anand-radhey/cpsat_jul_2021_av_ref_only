package day06;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Ex14C_ATACalcPOMDDUser {
	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@DataProvider
	public Object[][] dp() throws EncryptedDocumentException, InvalidFormatException {
		Object[][] data=null;
		try {
			data = utils.XLDataReaders.getExcelData("src\\test\\resources\\data\\Day10Exercise3.xlsx", "data");
			//data=utils.DataReaders.getExcelDataUsingPoi("src\\test\\resources\\data\\Day10Exercise3.xlsx", "data");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}

	@Test(dataProvider = "dp")
	public void f(String v1, String v2, String v3,String v4) {
		String input1 = v1;
		String input2 = v2;
		String operation = v3;
		String expectedResult = v4;
		
		driver.get("http://ata123456789123456789.appspot.com/");
		//System.out.println( v1  +  v2 + v3 +v4);
		
		EX14B_ATACalcPoM ataPage = new EX14B_ATACalcPoM(driver);
		//Ex14_ATA_Calc_POM ataPage = new Ex14_ATA_Calc_POM(driver);
		String actualResult=null;
		
		switch (v3.toUpperCase()) {

		case "MUL":
			
			System.out.println("Inside Mul");
			
			actualResult=ataPage.multiply(v1, v2);
			
	
			break;

		case "ADD":
			System.out.println("Inside Add");
			actualResult=ataPage.add(v1, v2);
			
			
			
			break;
		case "COMPARE":
			
			System.out.println("Inside Compare");
			actualResult=ataPage.compare(v1, v2);	
			break;			
		default:
			
			System.out.println("Inside Default");	
			break;
		}
       Assert.assertEquals(actualResult, v4,"result do not match");
		
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}