package day06;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Ex07_IMDB_NG {
	WebDriver driver = null;

	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome");
	}

	@Test
	public void movieTest() {
		String expDir = "Mastan";
		String expStar = "Shah Rukh Khan";
		String movie = "Baazigar";

		driver.get("https://www.imdb.com/");
		//*[@id="suggestion-search"]
		By byvar1 = By.xpath("//*[@id=\"suggestion-search\"]");
		WebElement weinput = driver.findElement(byvar1);

		weinput.sendKeys(movie);

		//*[@id="suggestion-search-button"]

		driver.findElement(By.xpath("//*[@id=\"suggestion-search-button\"]")).click();

		//*[@id="main"]/div/div[2]/table/tbody/tr[1]/td[2]/a

		driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a")).click();


		List<WebElement>   dirList	   = driver.findElements(By.xpath("//*[contains(text(), 'Director')]/following-sibling::*"));
		boolean dirflag = false;

		String msg = "";

		for(WebElement   dir   : dirList) {
			if (dir.getText().contains(expDir)) {
				System.out.println(" Found Expected Dir " + expDir);
				dirflag = true;
				break;
			}
		}

		if(dirflag) {
			System.out.println(" Found Expected Dir " + expDir);
			msg = msg + " Found Expected Dir " + expDir + ".\n ";
		} else {
			System.out.println(" NOT Found Expected Dir " + expDir);
			msg = msg + " NOT Found Expected Dir " + expDir + ".\n ";
		}

		// To process for Star
		List<WebElement>  starList = driver.findElements(By.xpath("//*[contains(text(), 'Stars')]/following-sibling::*"));

		boolean starflag = false;

		//String msg = "";

		for(WebElement   star   : starList) {
			if (star.getText().contains(expStar)) {
				System.out.println(" Found Expected star " + expStar);
				starflag = true;
				break;
			}
		}

		if(starflag) {
			System.out.println(" Found Expected star " + expStar);
			msg = msg + " Found Expected Star " + expStar + ".\n ";
		} else {
			System.out.println(" NOT Found Expected Star " + expStar);
			msg = msg + " NOT Found Expected Star " + expStar + ".\n ";
		}


		boolean actFlag = dirflag && starflag;
		Assert.assertEquals( actFlag, true, msg);

		System.out.println("Exit movieTest() ");



	}


	@AfterTest
	public void afterTest() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.quit();
	}

}
