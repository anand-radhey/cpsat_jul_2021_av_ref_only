package day06;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class ATA_Calc_Demo_NG {
	WebDriver driver = null;
	
	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome");

	}
	
	
	
  @Test
  public void testMult() {
		driver.get("http://ata123456789123456789.appspot.com/");
		String expRes = "150";

		//*[@id="ID_nameField1"]
		By byField1 = By.xpath("//*[@id=\"ID_nameField1\"]");
		WebElement weField1 = driver.findElement(byField1);

		//*[@id="ID_nameField2"]
		By byField2 = By.xpath("//*[@id=\"ID_nameField2\"]");
		WebElement weField2 = driver.findElement(byField2);

		//*[@id="gwt-uid-2"]
		By byMul = By.xpath("//*[@id=\"gwt-uid-2\"]");
		WebElement weMul = driver.findElement(byMul);
		
	
		

		//*[@id="ID_calculator"]
		By byCalc = By.xpath("//*[@id=\"ID_calculator\"]");
		WebElement weCalc = driver.findElement(byCalc);

		//*[@id="ID_nameField3"]

		By byRes = By.xpath("//*[@id=\"ID_nameField3\"]");
		WebElement weRes = driver.findElement(byRes);
	  
		
		

		weField1.clear();
		weField1.sendKeys("10");
		
		weField2.clear();
		weField2.sendKeys("15");
		
		weMul.click();
		weCalc.click();
		
		String actRes =  weRes.getAttribute("value");   //weRes.getText();
		
	
		
		Assert.assertEquals(actRes, expRes, "Incorrect Result from Application \n");
		
		System.out.println("The Result is = " + actRes + "   ...   ");
	  
	  
  }
 
  
  @AfterTest
	public void afterTest() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.quit();
	}

  
  
}
