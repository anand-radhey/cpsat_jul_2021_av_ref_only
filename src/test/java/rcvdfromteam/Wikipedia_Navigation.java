package rcvdfromteam;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
 
public class Wikipedia_Navigation {
	
 
    public static void main(String[] args) {
        String expTitle1 = "Wikipedia";
        String expTitle2 = "Wikipedia, the free encyclopedia";
        String expTitle3 = "Selenium - Wikipedia";
        String expTitle4 = "Selenium - Wikipedia";
 
       // System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
 
       // WebDriver driver = new ChromeDriver();
        
       WebDriver browser = utils.HelperFunctions.createAppropriateDriver("chrome",false);
        
        
       browser.get("https://www.wikipedia.org/");
 
        String pageTitle1 = browser.getTitle();
        System.out.println("Title of the page is : " + pageTitle1);
        if (pageTitle1.equals(expTitle1)) {
            System.out.println("Title1 Matches");
        } else {
            System.out.println("Title1 does not Match");
        }
        
        String filename1 = "src\\test\\resources\\screenshots\\WikiPage1_04.png";
        
        utils.HelperFunctions.captureScreenShot(browser, filename1);
 
        // By byvar = By.tagName("Strong");
        
      //*[@id="js-link-box-en"]/strong
 
        // By byvar = By.xpath("//*[@id=\"js-link-box-en\"]/strong");
        
        //By byvar = By.id("js-link-box-en");
        
        // By byvar = By.linkText("English");   Not working
        By byvar = By.className("link-box");
 
        WebElement weLink = browser.findElement(byvar);
        weLink.click();
        
String filename2 = "src\\test\\resources\\screenshots\\WikiPage2_04.png";
        
        utils.HelperFunctions.captureScreenShot(browser, filename2);
 
        By byvar1 = By.name("search");
 
        WebElement weinput = browser.findElement(byvar1);
        weinput.sendKeys("Selenium");
 
        By byvar2 = By.name("go");
 
        WebElement weMG = browser.findElement(byvar2);
        weMG.click();
 
        String pageTitle3 = browser.getTitle();
        System.out.println("Title of the page is : " + pageTitle3);
        if (pageTitle3.equals(expTitle3)) {
            System.out.println("Title3 Matches");
        } else {
            System.out.println("Title3 does not Match");
        }
 
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
 
        browser.navigate().back();
        String pageTitle2 = browser.getTitle();
        System.out.println("Title of the page is : " + pageTitle2);
        if (pageTitle2.equals(expTitle2)) {
            System.out.println("Title2 Matches");
        } else {
            System.out.println("Title2 does not Match");
        }
 
        //browser.navigate().to("https://www.google.com");
        
        
        
        browser.navigate().forward();
        String pageTitle4 = browser.getTitle();
        System.out.println("Title of the page is : " + pageTitle4);
        if (pageTitle4.equals(expTitle4)) {
            System.out.println("Title4 Matches ");
        } else {
            System.out.println("Title4 does not Match");
        }
        
        
 
        browser.quit();
 
    }
 
}