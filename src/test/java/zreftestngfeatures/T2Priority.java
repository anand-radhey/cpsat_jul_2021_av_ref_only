package zreftestngfeatures;

import org.testng.annotations.Test;

public class T2Priority {
	
	@Test
	public void first() {
	System.out.println("Inside first()  ");	
	}
	
	@Test(priority=-4)
	public void second() {
	System.out.println("Inside second()  ");	
	}
	
	@Test(priority=5)
	public void third() {
	System.out.println("Inside third()  ");	
	}
	
	@Test(priority=50)
	public void fourth() {
	System.out.println("Inside fourth()  ");	
	}
	
	@Test(priority=0)
	public void fifth() {
	System.out.println("Inside fifth()  ");	
	}
	
	
	

}
