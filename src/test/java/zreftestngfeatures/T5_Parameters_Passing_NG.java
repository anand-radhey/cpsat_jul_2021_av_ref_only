package zreftestngfeatures;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class T5_Parameters_Passing_NG {
	
	@Test
	@Parameters({"param1","param2"})
	public void first(String name1, String name2) {
		System.out.println("Inside first()  ");	
		System.out.println("Parameters passed are \n"+ name1 + "\t" + name2);
	}
	
	
	
	
	
	
	
	/*
	@Test
	@Parameters({"param1", "param2"})
	public void first(@Optional ("Honda") String name1, @Optional ("Toyota") String name2) {
	System.out.println("Inside first()  ");	
	System.out.println("Parameters passed are \n"+ name1 + "\t" + name2);
	}
	*/
	/*
	@Test
	public void second() {
	System.out.println("Inside second()  ");	
	}
	
	@Test
	@Parameters({"param3"})
	public void third(@Optional ("Firefox") String browserName) {
	System.out.println("Inside third()  ");	
	System.out.println("Parameters passed for Browser is :  \n"+ browserName );
	}
	
	@Test
	public void fourth() {
	System.out.println("Inside fourth()  ");	
	}
	
	@Test
	@Parameters({"param4"})
	//public void fifth(@Optional ("Chrome") String browser) {
	public void fifth(String browser) {
	System.out.println("Inside fifth()  ");	
	System.out.println("Parameters passed for Browser is :  \n"+ browser );
	}
	
	*/
	

}
