package zreftestngfeatures;

import org.testng.annotations.Test;
import org.testng.SkipException;
import org.testng.annotations.Test;
// Run t4_testng.xml for this class
// exploring include/exclude methods thru testng.xml

public class T4Skip_TestNG_NG {
	
	@Test
	public void first() {
	System.out.println("Inside first()  ");	
	}
	
	@Test
	public void second() {
	System.out.println("Inside second()  ");	
	}
	
	@Test
	public void third() {
	System.out.println("Inside third()  ");	
	}
	
	@Test
	public void fourth() {
	System.out.println("Inside fourth()  ");	
	throw new SkipException("Decided for Skipping - we are not ready for execution of this test.");
	}
	
	@Test
	public void fifth() {
	System.out.println("Inside fifth()  ");	
	}
	
	
	

}
