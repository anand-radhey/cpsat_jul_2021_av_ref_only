package testngfeatures;

import org.testng.annotations.Test;
import org.testng.SkipException;
import org.testng.annotations.Test;

public class T3Enable_Disable_Skip_NG {
	
	@Test(enabled = false)
	public void first() {
	System.out.println("Inside first()  ");	
	}
	
	@Test(enabled = true)
	public void second() {
	System.out.println("Inside second()  ");	
	}
	
	@Test(enabled = true)
	public void third() {
	System.out.println("Inside third()  ");	
	}
	
	@Test
	public void fourth() {
	System.out.println("Inside fourth()  ");	
	throw new SkipException("Decided for Skipping - we are not ready for execution of this test.");
	}
	
	@Test(enabled = true)
	public void fifth() {
	System.out.println("Inside fifth()  ");	
	}
	
	
	

}
