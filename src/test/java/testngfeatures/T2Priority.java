package testngfeatures;

import org.testng.annotations.Test;

public class T2Priority {
	
	@Test(priority=-2)
	public void first() {
	System.out.println("Inside first()  ");	
	}
	
	@Test(priority=1)
	public void second() {
	System.out.println("Inside second()  ");	
	}
	
	@Test
	public void third() {
	System.out.println("Inside third()  ");	
	}
	
	@Test(priority=1)
	public void fourth() {
	System.out.println("Inside fourth()  ");	
	}
	
	@Test(priority=10)
	public void fifth() {
	System.out.println("Inside fifth()  ");	
	}
	
	
	

}
