package testngfeatures;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class T5_Parameters_Passing_NG {
	
	@Test
	@Parameters({"param1","param2"})
	public void first(@Optional ("BMW") String name1,@Optional ("MERC") String name2) {
		System.out.println("Inside first()  ");	
		System.out.println("Parameters passed are \n"+ name1 + "\t" + name2 );
	}
	
	@Test
	@Parameters({"param3"})
	public void third(@Optional ("Firefox") String browserName) {
	System.out.println("Inside third()  ");	
	System.out.println("Parameters passed for Browser is :  \n"+ browserName );
	}
	
	

}
