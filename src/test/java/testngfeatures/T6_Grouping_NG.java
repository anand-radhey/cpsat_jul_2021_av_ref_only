package testngfeatures;

import org.testng.annotations.Test;

public class T6_Grouping_NG {
	
	@Test(groups = { "sanitytest", "fullregressiontest"})
	public void first() {
	System.out.println("Inside first()  ");	
	}
	
	@Test(groups = { "fullregressiontest" })
	public void second() {
	System.out.println("Inside second()  ");	
	}
	
	@Test(groups = { "fullregressiontest" })
	public void third() {
	System.out.println("Inside third()  ");	
	}
	
	@Test(groups = { "sanitytest"})
	public void fourth() {
	System.out.println("Inside fourth()  ");	
	}
	
	@Test(groups = { "sanitytest", "fullregressiontest"})
	public void fifth() {
	System.out.println("Inside fifth()  ");	
	}
	
	
	

}
