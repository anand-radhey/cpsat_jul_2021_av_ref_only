package day04;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Ex02_WindowSwitching_JU4.class, Ex06_SocialMedia_JU4.class, Google_JU4.class })
public class AllTests_JU4_Test_Suite {

}
