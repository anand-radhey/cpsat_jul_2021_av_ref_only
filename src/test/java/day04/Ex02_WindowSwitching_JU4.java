package day04;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Ex02_WindowSwitching_JU4 {
	WebDriver driver= null;
	@Before
	public void setUp() throws Exception {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome");
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() {
		driver.get("https://www.ataevents.org/");

		By byvar = By.xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[*]/div/div/div/div/div/figure/a");
		List<WebElement> welist = driver.findElements(byvar);

		for( WebElement ele   :  welist) {
			ele.click();
		}


		Set<String> setHandles = driver.getWindowHandles();

		for( String  handle     : setHandles) {
			System.out.println(handle);
			driver.switchTo().window(handle);
			String pageTitle = driver.getTitle();
			System.out.println("Title of the page : "+ pageTitle);	
		}

		String expTitle = "Registration | Global Testing Retreat 2021";




		for( String  handle     : setHandles) {
			//System.out.println(handle);
			driver.switchTo().window(handle);
			String pageTitle = driver.getTitle();
			if(pageTitle.equalsIgnoreCase(expTitle)) {
				System.out.println("I've switched to correct window ");
				System.out.println("Title of the page : "+ pageTitle);
				break;

			}	
		}

		driver.switchTo().frame(0);




		String expDate = "11 Dec 2021";
		// /html/body/app-root/app-ticket-page/app-event-data-panel/div/div/div/div[2]/div[2]/div
		By byvar2 = By.xpath("/html/body/app-root/app-ticket-page/app-event-data-panel/div/div/div/div[2]/div[2]/div");
		WebElement we = driver.findElement(byvar2);
		String text = we.getText();
		if(text.contains(expDate)) {
			System.out.println("Test Passed");

		} else {
			System.out.println("Test Failed");
		}


	}

}
