package day04;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Ex03_AnnaUniv {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://www.annauniv.edu/department/index.php");
		
		//*[@id="link3"]/strong    civil engg
		
		//*[@id="menuItemHilite33"]   iom
		
		By byvar1 = By.xpath("//*[@id=\"link3\"]/strong");
		WebElement wecivil = driver.findElement(byvar1);
		
		
		By byvar2 = By.xpath("//*[@id=\"menuItemHilite33\"]");
		WebElement weiom = driver.findElement(byvar2);
		
		
		Actions act = new Actions(driver);
		
		act.moveToElement(wecivil).moveToElement(weiom).click().build().perform();
		System.out.println(driver.getTitle());
		
		driver.quit();
		
		
		
		
		

	}

}
