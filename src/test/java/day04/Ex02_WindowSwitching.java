package day04;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Ex02_WindowSwitching {

	public static void main(String[] args) {
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("Chrome");
		driver.get("https://www.ataevents.org/");
		
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[1]/div/div/div/div/div/figure/a
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[2]/div/div/div/div/div/figure/a
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[3]/div/div/div/div/div/figure/a
		//*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[4]/div/div/div/div/div/figure/a
		
		By byvar = By.xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[*]/div/div/div/div/div/figure/a");
		List<WebElement> welist = driver.findElements(byvar);
		
		for( WebElement ele   :  welist) {
			ele.click();
			//String pageTitle = driver.getTitle();
			//System.out.println("Title of the page : "+ pageTitle);
		}
		
		
		Set<String> setHandles = driver.getWindowHandles();
		
		for( String  handle     : setHandles) {
			System.out.println(handle);
			driver.switchTo().window(handle);
			String pageTitle = driver.getTitle();
			System.out.println("Title of the page : "+ pageTitle);	
		}
		
		String expTitle = "Registration | Global Testing Retreat 2021";
		
		
		
		
		for( String  handle     : setHandles) {
			//System.out.println(handle);
			driver.switchTo().window(handle);
			String pageTitle = driver.getTitle();
			if(pageTitle.equalsIgnoreCase(expTitle)) {
				System.out.println("I've switched to correct window ");
				System.out.println("Title of the page : "+ pageTitle);
				break;
				
			}	
		}
		
		driver.switchTo().frame(0);
		
		
		
		
		String expDate = "11 Dec 2021";
		// /html/body/app-root/app-ticket-page/app-event-data-panel/div/div/div/div[2]/div[2]/div
		By byvar2 = By.xpath("/html/body/app-root/app-ticket-page/app-event-data-panel/div/div/div/div[2]/div[2]/div");
		WebElement we = driver.findElement(byvar2);
		String text = we.getText();
		if(text.contains(expDate)) {
			System.out.println("Test Passed");
			
		} else {
			System.out.println("Test Failed");
		}
		
		//driver.quit();
	}

}
