package day01;

public class Ex01_FindMax {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1 , num2;
		num1 = 100;
		num2 = 500;
		int max = -9999;
		//  num1 + num2   // Arithmatic expression
		// num1 > num2   // boolean expression
		
		if ( num1 > num2      ) {
			max = num1;
			
		} else {
			max = num2;
			
		}
		System.out.println("Maximum of two numbers is : "+ max);

	}

}
