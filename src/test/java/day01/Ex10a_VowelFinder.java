package day01;

public class Ex10a_VowelFinder {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char ch_input = '+';   //   "b"
		// a, e, i, o , u
		// Wrapper classes
		//   int   -- Integer
		// char  -- Character
		//Character chobj  = new Character(ch);
		//char ch = Character.toLowerCase(ch_input); // 'A'    'a'
		String strch = Character.toString(ch_input);  // 'a'      "a"
		String vowelString = "aeiouAEIOU";
		
		//String str = "Anand";
		//str.contains("and")
		
		//vowelString.contains(strch)
		
		//if (ch == 'a'  ||     ch == 'e' ||  ch == 'i' ||  ch == 'o' ||   ch == 'u' ) {
		if ( vowelString.contains(strch) ) {
			System.out.println(ch_input + " is a Vowel ");
		} else {
			System.out.println(ch_input + " is NOT a Vowel ");
		}

	}

}
