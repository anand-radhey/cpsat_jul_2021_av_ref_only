package day05;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Ex04_Alerts_NG {
	WebDriver driver = null;

	@BeforeTest
	public void beforeTest() {
		System.out.println("Inside beforeTest()");
		driver = utils.HelperFunctions.createAppropriateDriver("chrome");
	}

	@Test
	public void testJSAlert() {
		System.out.println("Inside testJSAlert()");
		
		
		driver.get("https://the-internet.herokuapp.com/javascript_alerts");

		//*[@id="content"]/div/ul/li[1]/button
		By byvar = By.xpath("//*[@id=\"content\"]/div/ul/li[1]/button");
		WebElement webutton = driver.findElement(byvar);

		webutton.click();
		//WebDriverWait wt = new WebDriverWait(driver, 15);
		//wt.until(ExpectedConditions.alertIsPresent());

		//wt.until(ExpectedConditions.alertIsPresent());
		
		Alert jsalert  = driver.switchTo().alert();

		jsalert.accept();

		//*[@id="result"]
		By byvarRes = By.xpath("//*[@id=\"result\"]");
		WebElement weRes = driver.findElement(byvarRes);

		String expResult = "You successfully clicked an alert";

		String actResult = weRes.getText();

		Assert.assertEquals(actResult, expResult,"Mismatch between Expected and Actual Result String");

	}

	@Test
	public void testJSConfirmOK() {
		System.out.println("Inside testJSConfirmOK()");

		driver.get("https://the-internet.herokuapp.com/javascript_alerts");

		By byvarConfirm = By.xpath("//*[@id=\"content\"]/div/ul/li[2]/button");
		WebElement weConfirmButton = driver.findElement(byvarConfirm);

		weConfirmButton.click();

		Alert jsConfirmOk = driver.switchTo().alert();
		jsConfirmOk.accept();

		// *[@id="result"]
		By byvarRes2 = By.xpath("//*[@id=\"result\"]");
		WebElement weRes2 = driver.findElement(byvarRes2);

		String expResult2 = "You clicked: Ok";
		String actResults2 = weRes2.getText();
		Assert.assertEquals(actResults2, expResult2);

	}

	@Test
	public void testJSConfirmCancel() {

		System.out.println("Inside testJSConfirmCancel()");
		driver.get("https://the-internet.herokuapp.com/javascript_alerts");

		By byvarConfirm = By.xpath("//*[@id=\"content\"]/div/ul/li[2]/button");
		WebElement weConfirmButton = driver.findElement(byvarConfirm);

		weConfirmButton.click();
		Alert jsConfirmCancel = driver.switchTo().alert();
		jsConfirmCancel.dismiss();

		// *[@id="result"]
		By byvarRes3 = By.xpath("//*[@id=\"result\"]");
		WebElement weRes3 = driver.findElement(byvarRes3);

		String expResult3 = "You clicked: Cancel";
		String actResult3 = weRes3.getText();

		Assert.assertEquals(actResult3, expResult3);
	}


	@AfterTest
	public void afterTest() {
		System.out.println("Inside afterTest()");
		driver.quit();
	}

}
