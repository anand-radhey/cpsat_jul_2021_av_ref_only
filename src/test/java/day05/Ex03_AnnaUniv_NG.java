package day05;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;

public class Ex03_AnnaUniv_NG {
	WebDriver driver = null;
	
  @Test
  public void myTest() {
	  System.out.println("Inside myTest()");
	  driver.get("https://www.annauniv.edu/department/index.php");
		
		//*[@id="link3"]/strong    civil engg
		
		//*[@id="menuItemHilite33"]   iom
		
		By byvar1 = By.xpath("//*[@id=\"link3\"]/strong");
		WebElement wecivil = driver.findElement(byvar1);
		
		
		By byvar2 = By.xpath("//*[@id=\"menuItemHilite33\"]");
		WebElement weiom = driver.findElement(byvar2);
		
		
		Actions act = new Actions(driver);
		
		act.moveToElement(wecivil).moveToElement(weiom).click().build().perform();
		System.out.println(driver.getTitle());

	  
  }
  @BeforeTest
  public void beforeTest() {
	  System.out.println("Inside beforeTest()");
	  driver = utils.HelperFunctions.createAppropriateDriver("chrome");
	  
	  
  }

  @AfterTest
  public void afterTest() {
	  System.out.println("Inside afterTest()");
	  driver.quit();
  }

}
