package agriddemo;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class AnotherExample {
	WebDriver driver = null;
	
	 @BeforeTest
	  public void beforeTest() throws MalformedURLException {
		 
		 driver = utils.RemoteBrowserCreation.createRemoteDriver("chrome", "http://192.168.29.143", "4444");
		 
	  }
	
  @Test
  public void f() {
	  driver.get("https://www.google.com");
	  //By byvar = By.xpath("//*[name='q']");
	  //By byvar = By.name("q");
	  
	  WebElement weinput = driver.findElement(By.name("q"));
	  
	  weinput.clear();
	  weinput.sendKeys("Alon Musk");
	  weinput.sendKeys(Keys.ENTER);
	  
	  System.out.println("Page Title is : " + driver.getTitle());
	  
	  utils.HelperFunctions.captureScreenShot(driver, "src\\test\\resources\\screenshots\\AlonMusk007.png");
	  
	  try {
		Thread.sleep(5000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	  
	  
  }
 
  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}
