package agriddemo;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class SelGridTest {
	
	WebDriver driver;
	
	 @BeforeTest
	  public void beforeTest() throws MalformedURLException {
		 
		 driver = utils.RemoteBrowserCreation.createRemoteDriver("chrome", "http://192.168.29.143", "4444");
		 
		 
		 
		 
	  }
	
	
  @Test
  public void myTestMethod() {
	  String expTitle = "Selenium Grid - Google Search";
	  driver.get("https://www.google.com/");
	  By byvar = By.name("q");
	  WebElement weinput = driver.findElement(byvar);
	  weinput.sendKeys("Selenium Grid");
	  
	  weinput.sendKeys(Keys.ENTER);
	  String actTitle = driver.getTitle();
	  
	  HelperFunctions.captureScreenShot(driver, "src/test/resources/google101.png");
	  
	  Assert.assertEquals(expTitle, actTitle, "NOT MATCHING - AV");
	  
	  
	  
	  
	  
	  
  }
 

  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}
