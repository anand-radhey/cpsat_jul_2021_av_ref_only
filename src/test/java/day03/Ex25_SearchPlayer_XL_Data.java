package day03;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import day02.Player;

public class Ex25_SearchPlayer_XL_Data {

	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
		// TODO Auto-generated method stub
		//int i = 10;
		String expName = "R Jadeja";
		String expID = "IN14";
		String expScore = "27";

		ArrayList<Player> arrList = new ArrayList<Player>();

		String fileName = "src\\test\\resources\\data\\Cricket_Score_India_Eng_SA.xlsx";
		String[] sheets = {"India", "England", "SA"};

		for(int k=0; k < sheets.length ; k++) {

			String sheetName = sheets[k];    //"India";

			String[][] datafromxl = utils.XLDataReaders.getExcelData(fileName, sheetName);

			for(int i=0; i < datafromxl.length ; i++ )
			{
				String name = datafromxl[i][0];
				String id =   datafromxl[i][1];
				String score = datafromxl[i][2];

				Player player = new Player(name,id,score);
				arrList.add(player);

			}

		}

		//System.out.println(arrList);

		/*for( int i = 0 ; i < arrList.size() ; i++) {

			System.out.println(arrList.get(i));
		}*/
		boolean flag = false;
		//for( Player p        : arrList ) {
		for( int i = 0 ; i < arrList.size() ; i++) {
			Player p = arrList.get(i);

			//System.out.println(p);
			String name = p.getName();
			String id = p.getId();
			String score = p.getScore();


			if ( expName.equalsIgnoreCase(name)  
					&&   expID.equalsIgnoreCase(id) 
					&&   expScore.equalsIgnoreCase(score)) {
				flag = true;

				break;


			}


		}

		if (flag) {
			System.out.println("Found Match for expected Player " + expName + "   ");
		} else {
			System.out.println("Found NO Match  for expected Player " + expName + "   ");
		}



	}

}
