package day03;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Google {

	public static void main(String[] args) {
		String expTitle = "google";
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		driver.get("http://www.google.com");
		
		String pageTitle = driver.getTitle();
		System.out.println("Title of the page is : "+ pageTitle);
		if (pageTitle.equals(expTitle)) {
			System.out.println("Test Passed");
		} else {
			System.out.println("Test Failed");
		}
		
		
		By byvar = By.xpath("//*[@name=\"q\"]");
		
		WebElement weinput = driver.findElement(byvar);
		weinput.sendKeys("CPSAT");
		
		By byvar2 = By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]");
		WebElement weButton = driver.findElement(byvar2);
		weButton.click();
		
		
		
		//        /html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input
		
		
		//driver.close();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		driver.quit();

	}

}
