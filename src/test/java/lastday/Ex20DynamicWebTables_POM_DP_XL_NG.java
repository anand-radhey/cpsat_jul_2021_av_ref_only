package lastday;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;

public class Ex20DynamicWebTables_POM_DP_XL_NG {
	WebDriver driver = null;
	ArrayList<Employee> listAllEmp;

	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome");
		driver.get("https://datatables.net/examples/styling/jqueryUI.html");
		Ex20WebTablePOM wtpom = new Ex20WebTablePOM(driver);
		listAllEmp = wtpom.readTable();
		for(Employee  emp     : listAllEmp) {
			System.out.println(emp);
		}
	}
	
	@Test(dataProvider = "dp")
	public void webTableSearching(String param1, String param2, String param3, String param4) {
		String Name = param1;
		String Position = param2;
		String Office = param3;
		String Age = param4;
		Employee searchEmp = new Employee(Name, Position, Office, Age);
		System.out.println("Searching for Employee " + searchEmp);
		boolean matchFound = false;
		for( Employee emp :  listAllEmp) {
			if( emp.getName().equals(Name) && emp.getPosition().equals(Position) &&
					emp.getOffice().equals(Office) && emp.getAge().equals(Age)) {
				matchFound = true;
				break;
			}
		}

		Assert.assertEquals(matchFound, true, "Employee Match not FOUND ");
	}

	@DataProvider
	public Object[][] dp() throws EncryptedDocumentException, InvalidFormatException, IOException {
		String fileName = "src\\test\\resources\\data\\WebTableData.xlsx";
		String sheetName = "searchdata";
		String[][] datafromxl = utils.XLDataReaders.getExcelData(fileName, sheetName);

		return datafromxl;
	}


	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
