package lastday;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Ex20WebTablePOM {
	WebDriver driver;
	WebDriverWait wait;
	By byPages = By.xpath("//*[@id=\"example_paginate\"]/span/a");
	By byRows = By.xpath("//*[@id=\"example\"]/tbody/tr");
	//By byCol1 = By.xpath("");
	ArrayList<Employee> listEmp;


	public Ex20WebTablePOM(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,10);
		listEmp = new ArrayList<Employee>();
	}

	public ArrayList<Employee> readTable() {

		List<WebElement> listPages = driver.findElements(byPages);
		//wait.until(ExpectedConditions.visibilityOfAllElements(listPages));
		for(int i= 1; i <= listPages.size() ; i++) {
			//driver.findElement(By.xpath("//*[@id=\\\"example_paginate\\\"]/span/a["+ i + "]")).click();
			List<WebElement> listRows =driver.findElements(byRows);
			//wait.until(ExpectedConditions.visibilityOfAllElements(listRows));
			for(int j = 1; j <= listRows.size() ; j++) {
				String name = driver.findElement(By.xpath("//*[@id=\"example\"]/tbody/tr["+ j + "]/td[1]")).getText();
				String position = driver.findElement(By.xpath("//*[@id=\"example\"]/tbody/tr["+ j + "]/td[2]")).getText();
				String office = driver.findElement(By.xpath("//*[@id=\"example\"]/tbody/tr["+ j + "]/td[3]")).getText();
				String age = driver.findElement(By.xpath("//*[@id=\"example\"]/tbody/tr["+ j + "]/td[4]")).getText();
				Employee emp = new Employee(name, position, office, age);
				listEmp.add(emp);	
			}
			if( i < listPages.size()) {
				driver.findElement(By.xpath("//*[@id=\"example_paginate\"]/span/a["+ (i+1) + "]")).click();
			}
		}

		return listEmp;

	}




}
