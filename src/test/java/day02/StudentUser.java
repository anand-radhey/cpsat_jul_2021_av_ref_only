package day02;

import day01.Student;

public class StudentUser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student st1 = new Student(100, "Anand"); //default constructor
		System.out.println("st1.id : " + st1.getId()   + "    st1.name : " +  st1.getName());
		
		
		Student st2 = new Student(200, "SreeDevi"); //default constructor
		System.out.println("st2.id : " + st2.getId()   + "    st2.name : " +  st2.getName());
		
		Student st3 = new Student(300, "Vijetha"); //default constructor
		System.out.println("st3.id : " + st3.getId()   + "    st3.name : " +  st3.getName());
		
		System.out.println(st1);
		System.out.println(st2);
		System.out.println(st3);
		
		
		

	}

}
