package day02;

import day01.Student;

public class OneDimArrays {

	public static void main(String[] args) {
		int x1 = 20;
		int x2 = 30;
		//int[] x = new int[25];
		int[] x = {20, 30, 40, 24, 56,78,100    };
		int len = x.length;
		System.out.println("Length of array = " + len);
		
		for(int i=0; i < len ; i++) {
			System.out.println(i + " ---     " + x[i]);
		}
		
		
		String[] strArray = {"India", "Japan", "China", "Canada"    } ;
		
		int lenstr = strArray.length;
System.out.println("Length of array = " + lenstr);
		
		for(int i=0; i < lenstr ; i++) {
			System.out.println(i + " ---     " + strArray[i]);
		}
		

		////////////////////////
		Student st1 = new Student(100, "Anand"); //default constructor
		System.out.println("st1.id : " + st1.getId()   + "    st1.name : " +  st1.getName());
		
		
		Student st2 = new Student(200, "SreeDevi"); //default constructor
		System.out.println("st2.id : " + st2.getId()   + "    st2.name : " +  st2.getName());
		
		Student st3 = new Student(300, "Vijetha"); //default constructor
		System.out.println("st3.id : " + st3.getId()   + "    st3.name : " +  st3.getName());
		
		Student[] stdArr = {st1, st2, st3};
		int std_len = stdArr.length;
		for(int i = 0; i < std_len ; i++) {
			System.out.println(i +  " ---     " + stdArr[i]);
		}
		
		
	}

}
