package day02;

public class TwoDimArraysDemo {

	public static void main(String[] args) {
		int[] row1 = {1000, 5000, 6000, 3000 };
		int[] row2 = {2000, 6000, 3000, 7500 };
		int[] row3 = {2100,2700,5000,1870 };
		
		int[][] sales = {row1, row2, row3};
		
		int nrow = sales.length;
		int ncol = sales[1].length;
		System.out.println("No. of Rows = " + nrow);
		System.out.println("No. of Cols = " + ncol);
		System.out.println("Q1 \t Q2 \t Q3 \t Q4");
		for(int i = 0 ; i < nrow ; i++) {
			for(int j=0; j < ncol;  j++) {
				System.out.print(sales[i][j] + "\t");	
			}
			System.out.println();	
		}
		
		
		
	}

}
