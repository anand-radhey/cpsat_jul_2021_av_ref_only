package day02;

public class Player {
	String name;
	String id;
	String score;
	
	

	public Player(String name, String id, String score) {
		super();
		this.name = name;
		this.id = id;
		this.score = score;
	}



	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}



	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}



	/**
	 * @return the score
	 */
	public String getScore() {
		return score;
	}



	/**
	 * @param score the score to set
	 */
	public void setScore(String score) {
		this.score = score;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Player [name=" + name + ", id=" + id + ", score=" + score + "]\n";
	}
	
	

}
